export const sizes = {
  1: '8px',
  2: '16px',
  3: '20px',
  4: '32px',
  5: '50px',
  6: '72px',
  7: '100px',
  8: '330px'
};
