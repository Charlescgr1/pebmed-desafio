export const fontSizes = {
  'x-small': '10px',
  small: '12px',
  normal: '14px',
  'x-normal': '16px',
  medium: '20px',
  'x-medium': '22px',
  'xx-medium': '24px',
  big: '30px',
  'x-big': '38px'
};
