export const spacings = {
  0: 0,
  small: '4px',
  normal: '12px',
  medium: '18px',
  'x-medium': '24px',
  big: '30px',
  'x-big': '42px'
};
