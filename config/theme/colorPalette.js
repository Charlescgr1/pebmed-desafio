export const customColors = {
  grey: {
    1: '#151516',
    2: '#666173',
    3: '#E1DEE8',
    4: '#C9C5D4',
    5: '#F4F3F6'
  },
  red: {
    1: '#E01B25',
    2: '#FA1E29'
  },
  indigo: {
    1: '#191847',
    2: '#2B2A7A',
    
  },
  orange: {
    1: '#F5850B',
    2: '#FA860A'
  },
  white: '#fff',
  black: '#000'
};

export const commomColors = {
  transparent: 'transparent',
  white: '#fff',
  black: '#000',
  lightGrey: '#f9f9f9'
};
