import { commomColors } from '.';

export const configBase = {
  'base-size': '8px',
  'base-font-size': '16px',
  'base-min-size': '48px',
  'base-multiplier': 1,
  'border-radius': '25px',
  'default-background': commomColors.white
};
