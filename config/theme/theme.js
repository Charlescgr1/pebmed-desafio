import {
  configBase, fontFamilies, spacings, fontSizes, sizes, shadows, customColors, commomColors, medias
} from '.';

export const theme = {
  name: 'PEB Bed',
  base: 'peb',
  configBase,
  fontFamilies,
  fontSizes,
  spacings,
  sizes,
  shadows,
  mainColor: 'indigo',
  secondaryColor: 'orange',
  customColors,
  commomColors,
  isColorizedMode: false,
  medias
};
