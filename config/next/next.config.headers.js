const {version} = require('../../package.json');

module.exports = async () => {
  return [
    {
      source: '/([A-Za-z0-9-_\\/]*)',
      headers: [
        {
          key: 'Surrogate-Key',
          value: `v${version} pages.html`
        },
        {
          key: 'Surrogate-Control',
          value: 'public, max-age=86400, stale-if-error=604800',
        }
      ]
    },
    {
      source: '/_next/image/(.*)',
      headers: [
        {
          key: 'Cache-Control',
          value:'public, max-age=2628000, s-maxage=2628000, stale-while-revalidate=86400',
        },
        {
          key: 'Surrogate-Control',
          value: 'public, max-age=2628000, stale-if-error=86400',
        },
        {
          key: 'Surrogate-Key',
          value: `v${version} images`
        }
      ]
    },
    {
      source: `/_next/data/([A-Za-z0-9-_\\/]+.json)`,
      headers: [
        {
          key: 'Surrogate-Control',
          value: 'public, max-age=86400, stale-if-error=604800',
        },
        {
          key: 'Surrogate-Key',
          value: `v${version} pages.json`
        }
      ]
    }
  ]
};
