import React from 'react';
import PropTypes from 'prop-types';

const buildColorClasses = (prefix, data) => {
  let propName = '';
  switch (prefix) {
    case 'c':
      propName = 'color';
      break;
    case 'bc':
      propName = 'background-color';
      break;
    default:
      propName = 'fill';
      break;
  }

  const colors = Object.keys(data.customColors).map(
    (color) => {
      let result = '';
      result += Object.keys(data.customColors[color]).map((variation) => ` .${prefix}--${color}-${variation}{${propName}:${data.customColors[color][variation]}}`);
      return result.trim();
    });
  return colors.toString().replace(/,/g, '');
};

function ColorsClasses({ data }) {
  return (
    <style jsx global>
      {`
          /* custom colors */
          ${buildColorClasses('c', data)}

          /* custom background-colors */
          ${buildColorClasses('bc', data)}

          /* custom Fill colors, for icons */
          ${buildColorClasses('f', data)}
        `}
    </style>
  );
}

ColorsClasses.propTypes = {
  /**
  * The object customColors.
  */
  data: PropTypes.any,
};

export default ColorsClasses;
