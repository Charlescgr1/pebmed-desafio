import React from 'react';
import TextClasses from './utils/_text';
import ColorsClasses from './utils/_colors';
import SpacingClasses from './utils/_spacing';
import HelpersClasses from './utils/_helpers';
import { theme } from '/config/theme';

function GlobalStyles() {
  return (
    <>
      <style jsx global>
      {`
          html,
          body {
              padding: 0;
              margin: 0;
              font-family: ${theme.fontFamilies.sans};
              background: ${theme.configBase['default-background']};
          }
          .center {
              justify-content: center;
              align-items: center;
              align-self: center;
              text-align: center;
          }
          .f--right {
            float: right 
          }
          .f--left {
            float: left 
          }
      `}
      </style>
      <ColorsClasses data={theme} />
      <SpacingClasses data={theme} />
      <TextClasses data={theme} />
      <HelpersClasses data={theme} />
    </>

  );
}

export default GlobalStyles;