import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';

import Container from '/components/atoms/Container';
import Image from '/components/atoms/Image';
import { publicRuntimeConfig } from '/utils/nextConfig';
function Header({
  logo
}) {
  const style = css.resolve`
    .header {
      position: absolute;
      width: 100%;
      height: 61px;
      left: 0px;
      top: 31px;
    }
  `;  
  return (
    <Container className="d--flex ai--center wrap">
      <header className={`${style.className} header`}>
        <Image src={logo} alt={publicRuntimeConfig?.APP_NAME} width={'40.8px'} height={'29px'} layout="fill" />
      </header>
      {style.styles} 
    </Container>
  );
}

Header.defaultProps = {
  logo: ''
};

Header.propTypes = {
  /**
   * logo
   */
   logo: PropTypes.string,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string
};

export default Header;
