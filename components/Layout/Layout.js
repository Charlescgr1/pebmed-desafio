import React from 'react';
import PropTypes from 'prop-types';
import Header from '/components/Header';
import Container from '/components/atoms/Container';
import css from 'styled-jsx/css';

const Layout = ({ children }) => {
  const style = css.resolve`
    .main {
      padding-top: 91px;
    }
  `;

  return (
    <>
      <Header
        logo={'/logo.svg'}
      />
      <Container className={`${style.className} main`}>
        <React.Fragment>{children}</React.Fragment>
      </Container>
      {style.styles}    
    </>
  );
};

Layout.propTypes = {
  /**
   * The children element
   */
  children: PropTypes.any
};

export default Layout;
