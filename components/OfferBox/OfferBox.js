import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';
import Col from '/components/atoms/Col';
import Box from '/components/atoms/Box';
import Row from '/components/atoms/Row';
import Span from '/components/atoms/Span';
import A from '/components/atoms/A';
import Button from '/components/atoms/Button';
import InputField from '/components/atoms/InputField';
import convertToMoney from '/utils/convertToMoney';

function OfferBox({
  offer, selected, installments, className, color, onClick
}) {
  const style = css.resolve`
    .offer{
      cursor: pointer;
      padding: ${theme.spacings.medium};
      border: 1px solid  ${createStyleClass(theme.customColors, color)};
      border-radius: 15px;
    }
  `;

  const offerInstallments = installments <= offer?.installments ? installments : 1

  const handleClick = (e) => {
    onClick && onClick(e);
  };

  return (
    <>
      <Col colSize={'12'} className={'mtb--normal'}>
        <A onClick={() => handleClick(offer)}>
          <Box className={`offer ${style.className} ${className}`}>
            <Row>
              <Col colSize={'10'}>
                <Box>
                  <Span className={'w--100 fw--bold'} fontSize={'normal'} textColor={color}>
                    {`${offer?.title} | ${offer?.description}`}
                  </Span>
                </Box>
                <Box>
                  <Span fontSize={'small'} textColor={color}>
                    {`De ${convertToMoney(offer?.fullPrice * offer?.installments)} | Por ${convertToMoney((offer?.fullPrice * offer?.installments) - (offer?.discountAmmount * offer?.installments))}`}
                  </Span>
                  <Button
                    className={`is--rounded is--x-small f--right hidden-mobile`}
                    color={'orange.1'}
                  >
                    {`${offer?.discountPercentage * 100}%`}
                  </Button>

                  <InputField
                    type="radio"
                    name="offer"
                    id={offer?.id}
                    value={offer}
                    checked={offer?.id === selected?.id}
                    className="d--inline-block f--right hidden-desktop"
                    onChange={() => handleClick(offer)}
                    borderColor={color}
                    textColor={color}
                  />                  
                </Box>
                <Box>
                  <Span className={'w--100'} fontSize={'x-small'} textColor={'orange.1'}>
                    {`${offerInstallments} x ${convertToMoney(((offer?.fullPrice * offer?.installments) - (offer?.discountAmmount * offer?.installments)) / offerInstallments)}/mês`}
                  </Span>
                </Box> 
              </Col>
              <Col colSize={'2'} className={'hidden-mobile'}>
                <InputField
                  type="radio"
                  name="offer"
                  id={offer?.id}
                  value={offer}
                  checked={offer?.id === selected?.id}
                  className="d--inline-block f--right"
                  onChange={() => handleClick(offer)}
                  borderColor={color}
                  textColor={color}
                />
              </Col>
            </Row>
          </Box>
        </A>
      </Col>      

      {/* common styles */}
      {style.styles}
    </>
  );
}

OfferBox.defaultProps = {
  installments: 1,
  color: 'indigo.1',
  className: ''
};

OfferBox.propTypes = {
  /**
   * The installments value
   */
   installments: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  
  /**
   * The object offer
   */
   offer: PropTypes.object.isRequired,

  /**
   * The object selected offer
   */
   selected: PropTypes.object.isRequired,   

  /**
   * The background color of the button
   */
  color: PropTypes.string,

  /**
   * The click function
   */
  onClick: PropTypes.func,

  /**
   * ClassNames
   */
  className: PropTypes.string,
};

export default OfferBox;
