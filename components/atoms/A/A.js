/* eslint-disable arrow-body-style */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function A({
  textColor,
  hoverColor,
  as,
  to,
  target,
  title,
  lineType,
  underlineColor,
  onClick,
  children,
  className
}) {
  const handleClick = () => {
    onClick && onClick();
  };

  const style = css.resolve`
    .default-a-link {
      transition: background-color 600ms ease, transform 200ms ease, box-shadow 400ms ease-out;
      word-break: break-word;
      transition: color 250ms ease-in-out;
      color: ${createStyleClass(theme.customColors, textColor)}
    }
    .is--underline{
      border-bottom-style: ${lineType};
      border-bottom-width: 1px;
      border-bottom-color: ${createStyleClass(theme.customColors, underlineColor)};
    }

    .has--animation:hover {
      box-shadow: -6px 6px 0px 0px rgba(0, 0, 0, 0.20);
    }

    .default-a-link:hover {
      transition: background-color 600ms ease, transform 200ms ease, box-shadow 400ms ease-out;
      ${hoverColor ? `color: ${createStyleClass(theme.customColors, hoverColor)};transition: color 250ms ease-in-out;` : ''}
    }

    a {
      text-decoration: none;
      outline: none;
    }    
  `;
  return (
    <>
      <Link
        href={to}
        as={as}
      >
        <a
          href={to}
          target={target}
          className={`default-a-link ${className} ${style.className}`}
          title={title}
          onClick={() => handleClick()}
        >
          {children || null}
        </a>
      </Link>
      {style.styles}
    </>
  );
}

A.defaultProps = {
  to: '#',
  target: '_self',
  hoverColor: 'grey.2',
  textColor: 'grey.1',
  underlineColor: 'grey.1',
  className: ''
};

A.propTypes = {

  /**
   * The anchor alias target when clicked (only next) eg.: 'home', 'about'. When
   * it is provided, the to prop will be ignored.
   */
  as: PropTypes.string,

  /**
   * The anchor link (pathname + prop)
   */
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),

  /**
   * The target of the link. eg.: 'blank'
   */
  target: PropTypes.oneOf(['_blank', '_self']),

  /**
   * The title of the link
   */
  title: PropTypes.string,

  /**
   * Text color
   */
  textColor: PropTypes.string,

  /**
   * hover color
   */
  hoverColor: PropTypes.string,

  /**
   * The type of underline eg: dotted, dashed, solid
   */
  lineType: PropTypes.oneOf(['dotted', 'dashed', 'solid']),

  /**
   * Text decoration, underline color
   */
   underlineColor: PropTypes.string,

  /**
   * The text for the Link
   */
  children: PropTypes.any,

  /**
   * The custom classNames
   */
  className: PropTypes.string,

  /**
   * The click function
   */
  onClick: PropTypes.func,  
};

export default A;
