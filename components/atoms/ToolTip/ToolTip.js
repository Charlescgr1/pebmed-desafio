import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function ToolTip({
  children, className
}) {
  const style = css.resolve`
    .tool-tip {
      min-width: 320px;
      max-width: 320px;
      left: calc(50% - 160px);
      top: 100%;
    }
    .tool-tip__content {
      border: 1px solid ${createStyleClass(theme.customColors, 'grey.1')};
    }
    .tool-tip__content::after {
      content: " ";
      width: 0;
      height: 0;
      position: absolute;
      top: -23px;
      left: 50%;
      border-width: 12px;
      border-style: solid;
      border-color: transparent transparent ${createStyleClass(theme.customColors, 'white')} transparent;
    }
    .tool-tip__content::before {
      content: " ";
      width: 0;
      height: 0;
      position: absolute;
      top: -24px;
      left: 50%;
      border-width: 12px;
      border-style: solid;
      border-color: transparent transparent  ${createStyleClass(theme.customColors, 'grey.1')} transparent;
    }
  `;

  return (
    <div
      className={`${style.className} tool-tip p--absolute zi--9 pt--big`}
      {...allowedProps}
    >
      <div className={`${style.className} tool-tip__content p--absolute bs--small br--small bc--white p--normal ${className}`}>
        {children}
      </div>
      {style.styles}
    </div>
  );
}

ToolTip.propTypes = {
  /**
   * The content
   */
  children: PropTypes.any.isRequired,

  /**
   * The custom className
   */
  className: PropTypes.string
};

export default ToolTip;
