/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import PropTypes from 'prop-types';
import ImageNext from 'next/image';

function Image(props) {
  return (
    <ImageNext
      {...props}
    />
  );
}

Image.defaultProps = {
  style: {},
  layout: 'responsive',
  alt: '',
};

Image.propTypes = {

  /**
   * The classname prop
   */
  className: PropTypes.string,
  /**
   * The alt tag
   */
  alt: PropTypes.string,

  /**
   * The image src
   */
  src: PropTypes.any.isRequired,

  /**
   * Custom styles
   */
  style: PropTypes.object,

  /**
   * For image (responsive)
   */
  layout: PropTypes.string,

  /**
   * Optional children that will be rendered when loading, instead of the constructicon loading dots
   * */
  children: PropTypes.any,

  /**
   * For image width
   */
   width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),

  /**
   * For image height
   */
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),  

};

export default Image;
