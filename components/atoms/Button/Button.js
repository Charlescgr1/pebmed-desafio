import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function Button({
  children, className, type, color, textColor, onClick, disabled
}) {
  const style = css.resolve`
    .button{
      display: flex;
      align-items: center;
      appearance: none;
      outline: none;
      cursor: pointer;
      font-family: ${theme.fontFamilies.sans};
      align-items:center;
      transition: background 200ms ease, transform 200ms ease, box-shadow 400ms ease-out;
    }

    .button:focus,
    .button:active {
      outline: none;
    }

    .is--rounded {
      border-radius: ${theme.configBase['border-radius']};
    }

    .hover-effect--shadow {
      box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.3);
    }

    .hover-effect--shadow:hover:not(.is--disabled) {
      box-shadow: -5px 5px 0px 0px rgba(0, 0, 0, 0.3);
    }

    .button.is--disabled,
    .button:disabled {
      cursor: not-allowed;
      opacity: 0.3;
    }

    // sizes
    
    .is--x-small {
      font-size: ${theme.fontSizes['x-small']};
      padding-top: ${theme.spacings['x-small']};
      padding-bottom: ${theme.spacings['x-small']};
      padding-left: ${theme.spacings['x-small']};
      padding-right: ${theme.spacings['x-small']};
    }
    .is--small {
      font-size: ${theme.fontSizes.small};
      padding-top: ${theme.spacings.small};
      padding-bottom: ${theme.spacings.small};
      padding-left: ${theme.spacings.normal};
      padding-right: ${theme.spacings.normal};
    }
    .is--normal {
      padding-top: ${theme.spacings.normal};
      padding-bottom: ${theme.spacings.normal};
      padding-left: ${theme.spacings.big};
      padding-right: ${theme.spacings.big};
      font-size: ${theme.fontSizes.normal};
      line-height:1.5
    }
    .is--medium {
      padding-top: ${theme.spacings.normal};
      padding-bottom: ${theme.spacings.medium};
      padding-left: ${theme.spacings['x-medium']};
      padding-right: ${theme.spacings['x-medium']};
      font-size: ${theme.fontSizes.normal};
    }
    .is--big {
      padding-top: ${theme.spacings.big};
      padding-bottom: ${theme.spacings.big};
      padding-left: ${theme.spacings['x-big']};
      padding-right: ${theme.spacings['x-big']};
      font-size: ${theme.fontSizes.big};
    }

    .is--full-rounded {
      padding: 0;
      border-radius: 50%;
    }

    @media only screen and (max-width: ${theme.medias.tablet}){
      .button-filled {
        position: inherit;
      }
    }    
  `;
  const colors = css.resolve`
    .button__background-color {
      background-color: ${createStyleClass(theme.customColors, color)};
      border-color: ${createStyleClass(theme.customColors, color)};
    }
    .button__text-color {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
  `;

  const handleClick = () => {
    if (!disabled && onClick) {
      onClick && onClick();
    }
  };

  return (
    <>
      <button
        className={`button ${style.className} ${colors.className}  button__background-color button__text-color ${className} ${disabled ? 'is--disabled' : ''}`}
        // eslint-disable-next-line react/button-has-type
        type={type || 'button'}
        onClick={() => handleClick()}
      >
        {children}
      </button>

      {/* common styles */}
      {style.styles}
      {colors.styles}
    </>
  );
}

Button.defaultProps = {
  type: 'button',
  color: 'indigo.1',
  textColor: 'white',
  className: '',
  disabled: false
};

Button.propTypes = {
  /**
   * The type the button
   */
  type: PropTypes.oneOf(['button', 'submit', 'reset']),

  /**
   * The background color of the button
   */
  color: PropTypes.string,

  /**
   * The text color of the button
   */
  textColor: PropTypes.string,

  /**
   * The text for the button
   */
  children: PropTypes.any.isRequired,

  /**
   * The click function
   */
  onClick: PropTypes.func,

  /**
   * ClassNames
   */
  className: PropTypes.string,

  /**
   * Trigger the disable state
   */
   disabled: PropTypes.bool,  
};

export default Button;
