import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function Label({
  children, inputId, required, className, textColor
}) {
  const style = css.resolve`
    label {
      cursor: pointer;
      font-family: ${theme.fontFamilies.sans};
      font-size: ${theme.fontSizes.small};
      color: ${createStyleClass(theme.customColors, textColor)};
    }
  `;
  return (
    <>
      <label
        className={`${style.className} label ${className}`}
        htmlFor={inputId}
      >
        {children}
        {required && <span title="*">*</span>}
      </label>    
      {style.styles}
    </>
  );
}

Label.defaultProps = {
  textColor: 'grey.2',
  className: ''
};

Label.propTypes = {
  /**
   * The label
   */
  children: PropTypes.any.isRequired,

  /**
   * The className of the label
   */
  className: PropTypes.string,

  /**
   * The id of the related input
   */
  inputId: PropTypes.string,

  /**
   * The color of text
   */
  textColor: PropTypes.string,

  /**
   * Whether the field is required
   */
  required: PropTypes.bool
};

export default Label;
