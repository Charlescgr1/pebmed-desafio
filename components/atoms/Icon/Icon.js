import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';
import { Icon as Iconify, InlineIcon } from '@iconify/react';
import usedIcon from './usedIcons';

function Icon({
  name,
  prefix,
  color,
  size,
  customWidth,
  inline,
  rotate,
  className
}) {
  let width = '';

  // eslint-disable-next-line
  const i = usedIcon[`${prefix}-${name}`];

  switch (size) {
    case 'small':
      width = theme.sizes['2'];
      break;
    case 'normal':
      width = theme.sizes['3'];
      break;
    case 'medium':
      width = theme.sizes['4'];
      break;
    case 'x-medium':
      width = theme.sizes['5'];
      break;
    case 'big':
      width = theme.sizes['6'];
      break;
    case 'x-big':
      width = theme.sizes['7'];
      break;
    case 'custom':
      width = customWidth;
      break;
    default:
      width = theme.sizes['3'];
  }

  const style = css.resolve`
    svg.iconify {
      min-width: ${width};
    }
  `;

  const renderIcon = () => {
    if (inline) {
      return (
        <InlineIcon
          icon={i?.default}
          width={width}
          className={`iconify ${style.className} ${className}`}
          color={createStyleClass(theme.customColors, color)}
          rotate={rotate}
        />
      );
    }

    return (
      <Iconify
        icon={i?.default}
        width={width}
        className={`iconify ${style.className} ${className}`}
        color={createStyleClass(theme.customColors, color)}
        rotate={rotate}
      />
    );
  };

  return (
    <>
      {renderIcon()}
      {style.styles}
    </>
  );
}

Icon.defaultProps = {
  prefix: 'bx',
  color: 'grey.1',
  inline: false
};

Icon.propTypes = {
  /**
   * The name of the icon
   */
  name: PropTypes.string.isRequired,

  /**
   * The prefix eg: 'bx', 'bxs', 'bxl'
   */
  prefix: PropTypes.string,

  /**
   * The size of the icon. eg: 'medium, big or x-big'
   */
  size: PropTypes.string,

  /**
   * The custom size of the icon. eg: '100px'
   */
  customWidth: PropTypes.string,

  /**
   * The color of the icon
   */
  color: PropTypes.string.isRequired,

  /**
   * If the icon will be displayed inline or in block
   */
  inline: PropTypes.bool,

  /**
   * The rotation angle, 90|180|270
   */
  rotate: PropTypes.string,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string
};

export default Icon;
