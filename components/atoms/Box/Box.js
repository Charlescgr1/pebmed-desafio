/* eslint-disable react/destructuring-assignment */
import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

const BoxComponent = forwardRef(({ type: Box, children, className, ...props }, ref) => {
  return (
    <Box
      ref={ref}
      className={className}
    >
      {!props?.dangerouslySetInnerHTML ? children : null}
    </Box>
  );
});

BoxComponent.displayName = 'BoxComponent';

BoxComponent.defaultProps = {
  type: 'div',
  className: ''
};

BoxComponent.propTypes = {
  /**
   * The type for the box section, div etc.
   */
  type: PropTypes.oneOf(['section', 'div', 'main']),

  /**
   * The children element
   */
  children: PropTypes.any,

  /**
   * The dangerouslySetInnerHTML element
   */
  dangerouslySetInnerHTML: PropTypes.object,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string,

  /**
   * The ref prop.
   */
  ref: PropTypes.any
};

export default BoxComponent;
