/* eslint-disable arrow-body-style */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/destructuring-assignment */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Span from '/components/atoms/Span';

function A({
  textColor,
  fontSize,
  message,
  validate,
  value,
  callBack,
  showMessage,
  className
}) {
  const [isValid, setIsValid] = useState(true)
  useEffect(() => {
    const v = validate(value)
    setIsValid(v);
    callBack(v)
  }, [value]);

  return (
    <Span textColor={textColor} fontSize={fontSize} className={className}>
      {(!isValid && showMessage) && message}
    </Span>
  );
}

A.defaultProps = {
  textColor: 'red.1',
  fontSize: 'x-small',
  showMessage: false,
  className: ''
};

A.propTypes = {

  /**
   * Text color
   */
  textColor: PropTypes.string,

  /**
   * Text size
   */
   fontSize: PropTypes.string,  

  /**
   * The custom classNames
   */
  className: PropTypes.string,

  /**
   * The click function
   */
   validate: PropTypes.func.isRequired,  

  /**
   * Show message error
   */
   showMessage: PropTypes.bool,     
   
};

export default A;
