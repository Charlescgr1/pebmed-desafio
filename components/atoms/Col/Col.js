import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';

function Col({
  children, className, colSize
}) {
  const style = css.resolve`
    @media only screen and (min-width: ${theme.medias.tablet}){
      .column__aside{
        width:301px;
      }
    }

    /* MOBILE AND SMALL TABLETS */
    .column {
      box-sizing: border-box;
      flex-basis: 100%;
      max-width: 100%;
      width: 100%;
      min-width: 0;
      float: left
    }

    /* IPADS and ALL DESKTOP SCREEN */
    @media only screen and (min-width: 900px) {
      .column{
        flex: 1 0 100%;
      }
      .column--1 {
        max-width: 8.3333333333%;
        width: 8.3333333333%;
      }
      .column--2 {
        max-width: 16.6666666667%;
        width: 16.6666666667%;
      }
      .column--3 {
        max-width: 25%;
        width: 25%;
      }
      .column--4 {
        max-width: 33.3333333333%;
        width: 33.3333333333%;
      }
      .column--5 {
        max-width: 41.6666666667%;
        width: 41.6666666667%;
      }
      .column--6 {
        max-width: 50%;
        width: 50%;
      }
      .column--7 {
        max-width: 58.3333333333%;
        width: 58.3333333333%;
      }
      .column--8 {
        max-width: 66.6666666667%;
        width: 66.6666666667%;
      }
      .column--9 {
        max-width: 75%;
        width: 75%;
      }
      .column--10 {
        max-width: 83.3333333333%;
        width: 83.3333333333%;
      }
      .column--11 {
        max-width: 91.6666666667%;
        width: 91.6666666667%;
      }

      .column--12 {
        flex-basis: 100%;
      }
      .column--auto{
        flex: 1 0 0;
        max-width:100%;
        min-width:0;
      }
      .column--fixed {
        flex: initial;
      }
    }    
  `;

  return (
    <div
      className={`${style.className} column column--${colSize} ${className}`}
    >
      {children}
      {style.styles}
    </div>
  );
}

Col.defaultProps = {
  className: ''
};

Col.propTypes = {
  /**
   * The type of column size 1,2,6,10 | auto | fixed
   */
  colSize: PropTypes.string,

  /**
   * The children element
   */
  children: PropTypes.any.isRequired,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string
};

export default Col;
