import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';


function Row({
  children, className
}) {
  const style = css.resolve`
    .row{
      box-sizing: border-box;
      flex-direction: row;
      flex-wrap: wrap;
      display: flex;
      width: 100%;
    }    
  `;  
  return (
    <>
      <div
        className={`${style.className} row ${className}`}
      >
        {children}
      </div>
      {style.styles}    
    </>
  );
}

Row.defaultProps = {
  className: ''
};

Row.propTypes = {
  /**
   * The children element
   */
  children: PropTypes.any.isRequired,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string
};

export default Row;
