import React, { useState } from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function InputField({
  id,
  type,
  label,
  placeholder,
  name,
  onBlur,
  onChange,
  value,
  required,
  input,
  ref,
  checked,
  textColor,
  borderColor,
  className,
  maxLength
}) {

  const inputId = id || name || (input && input.name);
  const Tag = type === 'textarea' ? 'textarea' : 'input';
  const isBoolean = (typeOf) => ['radio', 'checkbox'].indexOf(typeOf) > -1;
  const handleOnChange = onChange || (input && input.onChange) || null;
  const handleOnBlur = onBlur || (input && input.onBlur) || null;
  const inputName = name || (input && input.name);

  const style = css.resolve`
    .is--small {
      padding: ${theme.spacings.small};
    }

    .is--medium {
      padding: ${theme.spacings.medium};
    }

    .is--full {
      width: 100%;
    }

    .is--big {
      padding: ${theme.spacings.big};
    }

    .input-field {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
    ::-webkit-input-placeholder {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
    ::-moz-placeholder {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
    :-ms-input-placeholder {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
    :-moz-placeholder {
      color: ${createStyleClass(theme.customColors, textColor)};
    }
  `;

  const borderColors = css.resolve`
    input, input:focus,
    textarea, textarea:focus {
      border-color: transparent;
      border-bottom: 1px solid ${createStyleClass(theme.customColors, textColor)};
      color: ${createStyleClass(theme.customColors, borderColor)};
      height: ${theme.sizes[5]};
      font-size: ${theme.fontSizes['x-normal']};
      accent-color: ${createStyleClass(theme.customColors, textColor)};
    }
    input[type="checkbox"] ~ :global(.label:before),
    input[type="radio"] ~ :global(.label:before) {
      border: 1px solid ${createStyleClass(theme.customColors, borderColor)};
    }
  `;

  const renderInput = () => {
    const component = (
      <>
        <Tag
          id={inputId}
          className={`input-field ${className} ${borderColors.className}`}
          name={inputName}
          value={value}
          checked={checked}
          onChange={(e) => handleOnChange && handleOnChange(isBoolean(type) ? e.target.checked : e.target.value)}
          onBlur={(e) => handleOnBlur && handleOnBlur(isBoolean(type) ? e.target.checked : e.target.value)}
          placeholder={placeholder}
          type={type}
          required={required}
          ref={ref}
          maxLength={maxLength}
          {...input}
        />
      </>
    );
    return component;
  };

  return (
    <React.Fragment>
      {renderInput()}

      {/* custom styles */}
      {style.styles}
      {borderColors.styles}
    </React.Fragment>
  );
}

InputField.defaultProps = {
  validate: true,
  type: 'text',
  textColor: 'grey.4',
  borderColor: 'grey.4',
  maxLength: 40
};

InputField.propTypes = {
  /**
   * The label of input
   */
  label: PropTypes.string,

  /**
   * The className prop
   */
  className: PropTypes.string,

  /**
   * The style prop
   */
  style: PropTypes.object,

  /**
   * Render size of the field
   */
  size: PropTypes.string,

  /**
   * Indicate if should render message validators
   */
  validate: PropTypes.bool,

  /**
   * Indicate if should render message validators
   */
  checked: PropTypes.bool,

  /**
   * The placeholder of the field
   */
  placeholder: PropTypes.string,

  /**
   * The placeholder of the field
   */
  borderColor: PropTypes.string,

  /**
   * The color of text and placeholder
   */
  textColor: PropTypes.string,

  /**
   * The id of the field
   */
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /**
   * The name of the field
   */
  name: PropTypes.string,

  /**
   * The current value
   */
  value: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string
  ]),

  /**
   * An input element, usually used together redux-form
   */
  input: PropTypes.oneOfType([PropTypes.element, PropTypes.object]),

  /**
   * If field is required.
   */
  required: PropTypes.bool,

  /**
   * The type of field
   */
  type: PropTypes.string,

  /**
   * The change handler that will receive the updated value as it's only param
   */
  onChange: PropTypes.func,

  /**
   * The blur handler that will receive the updated value as it's only param
   */
  onBlur: PropTypes.func,

  /**
   * Full width box
   */
  full: PropTypes.bool,

  /**
   * An ref element
   */
  ref: PropTypes.any,

  /**
   * Max length in input
   */
   maxLength: PropTypes.number,
  
};

export default InputField;
