import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function Heading({
  children,
  className,
  textColor,
  type: Heading,
}) {
  const style = css.resolve`
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-family: ${theme.fontFamilies.sans};
      color: ${createStyleClass(theme.customColors, textColor)}
    }

    h1 {
      font-size: ${theme.fontSizes['x-big']};
      line-height: 1;
      letter-spacing: -0.02em;
      font-weight: 400;
    }

    h2 {
      font-size: ${theme.fontSizes.big};
      line-height: 1.25;
      letter-spacing: 0.01em;
      font-weight: 400;
    }

    h3 {
      font-size: ${theme.fontSizes['xx-medium']};
      line-height: 1.2;
      letter-spacing: 0.02em;
      font-weight: 400;
    }

    h4 {
      font-size: ${theme.fontSizes.medium};
      line-height: 26.4px;
      letter-spacing: 0.01em;
      font-weight: 400;
    }

    h5 {
      font-size: ${theme.fontSizes.normal};
      line-height: 1.5;
      letter-spacing: 0.04em;
      font-weight: 400;
    }

    h6 {
      font-size: ${theme.fontSizes.normal};
      line-height: 2;
      letter-spacing: 0.04em;
      font-weight: 400;
    }
  `;

  return (
    <>
      <Heading
        className={`${style.className} ${className}`}
      >
        {children}
      </Heading>
      {style.styles}
    </>
  );
}

Heading.defaultProps = {
  type: 'h1',
  className: '',
  textColor: 'grey.1'
};

Heading.propTypes = {
  /**
   * The heading level type
   */
  type: PropTypes.string,

  /**
   * The text for the Link
   */
   children: PropTypes.any.isRequired,

   /**
    * Text color
    */
   textColor: PropTypes.string,
 
   /**
    * The custom classNames
    */
   className: PropTypes.string  
};

export default Heading;
