import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function Span({ textColor, fontSize, className, children }) {
  const style = css.resolve`
    .span {
      font-size: ${theme.fontSizes[fontSize]};
      color: ${createStyleClass(theme.customColors, textColor)};
    }
  `;

  return (
    <>
      <span
        className={`${style.className} span ${className}`}
      >
        {children}
      </span>
      {style.styles}
    </>
  );
}

Span.defaultProps = {
  textColor: 'grey.1',
  fontSize: 'x-normal',
  className: ''
};

Span.propTypes = {

  /**
   * The text for the Link
   */
  children: PropTypes.any,

  /**
   * Text color
   */
  textColor: PropTypes.string,

  /**
   * Text size
   */
   fontSize: PropTypes.string,  

  /**
   * The custom classNames
   */
  className: PropTypes.string
};

export default Span;
