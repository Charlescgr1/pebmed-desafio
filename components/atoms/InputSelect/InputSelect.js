import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';
import { theme } from '/config/theme';
import { createStyleClass } from '/utils/createStyleClass';

function InputSelect({
  id,
  name,
  options,
  className,
  textColor,
  borderColor,
  onChange
}) {
  const handleSelected = (e) => {
    onChange && onChange(e.target.value);
  };

  const borderColors = css.resolve`
    select, select:focus {
      border-color: transparent;
      border-bottom: 1px solid ${createStyleClass(theme.customColors, textColor)};
      color: ${createStyleClass(theme.customColors, borderColor)};
      height: ${theme.sizes[5]};
      font-size: ${theme.fontSizes['x-normal']};
    }
  `;  
  return (
    <>
      <select
        id={id}
        className={`input-select ${className} ${borderColors.className}`}
        name={name}
        onChange={(e) => handleSelected(e)}
      >
        {options.map(({ value, label }) => (
          <option key={label} value={value}>{label}</option>
        ))}
      </select>
      {borderColors.styles}    
    </>
  );
}

InputSelect.defaultProps = {
  className: '',
  textColor: 'grey.4',
  borderColor: 'grey.4'  
};

InputSelect.propTypes = {
  /**
   * The className prop
   */
  className: PropTypes.string,

  /**
   * The name of the field
   */
  name: PropTypes.string.isRequired,

  /**
   * The options list
   */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      label: PropTypes.string,
    })).isRequired,

  /**
   * The id of the field
   */
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /**
   * The change handler that will receive the updated value as it's only param
   */
   onChange: PropTypes.func,  

};

export default InputSelect;
