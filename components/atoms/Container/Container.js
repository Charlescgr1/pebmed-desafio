import React from 'react';
import PropTypes from 'prop-types';
import css from 'styled-jsx/css';

function Container({
  children, className
}) {
  const style = css.resolve`
    .container {
      box-sizing: border-box;
      max-width: 100%;
      width: 100%;
    }
    .wrap{
      max-width: 900px;
      margin: 0 auto;
    }
  `;

  return (
    <>
      <div
        className={`${style.className} container ${className}`}
      >
        {children}
        {style.styles}
      </div>
      {style.styles}
    </>
  );
}

Container.defaultProps = {
  className: 'wrap'
};

Container.propTypes = {
  /**
   * The children element
   */
  children: PropTypes.any.isRequired,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string
};

export default Container;
