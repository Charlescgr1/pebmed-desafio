import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
function PageWrapper({
  children, ...props
}) {
  const { config } = props;

  const handleCreateHead = ({
    title, meta = [],
  }) => (
    <Head>
      <title>{title}</title>
      {
        meta.map((item) => (
          item && item.name && item.content
            ? <meta name={item.name} content={item.content} key={item.name} />
            : null
        ))
      }
    </Head>
  );

  return (
    <>
      {handleCreateHead(config)}
      {children}
    </>
  );
}

PageWrapper.defaultProps = {
  config: {
    title: 'Title of page',
    meta: [
      {
        name: 'description',
        content: 'Description of the page',
      },
    ],
  },
};

PageWrapper.propTypes = {
  /**
   * The custom configuration of the page wrapper.
   */
  config: PropTypes.shape({
    title: PropTypes.string, // Title of the page
    meta: PropTypes.array, // Config SEO meta and description
  }),

  /**
   * The children elements / content
   */
  children: PropTypes.any.isRequired,

  /**
   * The custom classname prop.
   */
  className: PropTypes.string,
};

export default PageWrapper;
