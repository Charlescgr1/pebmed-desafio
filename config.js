/**
 * The application config file.
 * Contains whole configuration info from the application.
 */

 const { version, name } = require('./package.json');

const Config = {
  VERSION: version,
  APP_NAME: name,
  APP_LOCALE: 'pt-br',
  API_URL: 'https://private-0ced4-pebmeddesafiofrontend.apiary-mock.com/',
};

module.exports = Config;
