/* eslint-disable no-console */
/* eslint-disable array-callback-return */
import axios from 'axios';
import { publicRuntimeConfig } from '/utils/nextConfig';
import createLogger from '/utils/log';

const logger = createLogger('API');

function Api() {
  function toQuery(obj) {
    if (!obj) {
      return '';
    }
    return Object.entries(obj)
      .map(([key, val]) => `${key}=${val}`)
      .join('&');
  }

  function request(path, { method, body, config = {} }) {
    const BASE_URL = `${publicRuntimeConfig.API_URL}/`;

    if (method === 'GET' && body) {
      path = [path, toQuery(body)].join('?');
      body = undefined;
    } else {
      body = JSON.stringify(body);
    }

    const response = axios({
      method,
      url: `${BASE_URL}${path}`,
      data: body,
      ...config
    });

    return response;
  }

  function getOffers() {
    return request(`/offer`, { method: 'GET' });
  }

  function subscription(data) {
    return request(`/subscription`, { method: 'POST', body: data });
  }

  return {
    getOffers,
    subscription
  };
}

function handleApiData(data) {
  if (data.data && data.data.status && [500].indexOf(data.data.status) > -1) {
    logger.warn('The request returns an invalid code status.');
    return [];
  }
  return data;
}

export const fetchOffers = async () => {
  try {
    const { data } = await Api().getOffers();
    return handleApiData(data);
  } catch (e) {
    logger.warn(e);
    return [];
  }
};

export const subscription = async (v) => {
  try {
    const { data } = await Api().subscription(v);
    return handleApiData(data);
  } catch (e) {
    logger.warn(e);
    return [];
  }
};
