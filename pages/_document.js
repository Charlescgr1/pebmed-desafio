import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';
import { publicRuntimeConfig } from '/utils/nextConfig';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  // eslint-disable-next-line class-methods-use-this
  get htmlAttrComponents() {
    return {
      dir: 'ltr',
      lang: publicRuntimeConfig.APP_LOCALE
    };
  }

  render() {
    return (
      <Html {...this.htmlAttrComponents}>
        <Head>
          <link rel="preconnect" href="https://fonts.google.com" />
          <link href="https://fonts.google.com/specimen/DM+Sans?preview.text_type=custom" rel="stylesheet" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
