import React from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import {
  fetchOffers
} from '/lib/apiClient';

const ErrorScreen = dynamic(() => import('/screens/Error'));
const LoadingScreen = dynamic(() => import('/screens/Loading'));
const PlansScreen = dynamic(() => import('/screens/Plans'));
const CongratulationsScreen = dynamic(() => import('/screens/Congratulations'));

const Slug = ({ pagePath, ...props }) => {
  const router = useRouter();
  if (router.isFallback) {
    return <LoadingScreen />;
  }
  return (
    <>
      {(() => {
        switch (pagePath) {
          case 'subscription': {
            return <PlansScreen {...props} />;
          }
          case 'congratulations': {
            return <CongratulationsScreen {...props} />;
          }
          default: {
            return <ErrorScreen {...props} />;
          }
        }
      })()}
    </>
  );
};

export async function getStaticProps({ params }) {
  let pagePath = 'error'
  let offers = []
  if (params?.slug?.includes('assinatura')) {
    pagePath = 'subscription';
    offers = await fetchOffers();
  } else if (params?.slug?.includes('parabens')) {
    pagePath = 'congratulations'
  }

  switch (pagePath) {
    case 'subscription': {
      return {
        props: {
          pagePath,
          offers,
          subscription: {},
          title: 'Planos',
        },
        revalidate: 60
      };
    }
    case 'congratulations': {
      return {
        props: {
          pagePath,
          subscription: {},
          title: 'Parabéns',
        },
        revalidate: 60
      };
    }
    default: {
      return {
        props: {
          statusCode: 404,
          title: 'Error page',
          pagePath
        },
        revalidate: 60 // In seconds
      };
    }
  }
}

export async function getStaticPaths() {
  const paths = [
    { params: { slug: ['assinatura'] } },
    { params: { slug: ['parabens'] } },
  ];

  return { paths, fallback: 'blocking' };
}

Slug.propTypes = {
  pagePath: PropTypes.string,
};
export default Slug;
