
import { MainContextProvider } from '/contexts/main.context';
import Layout from '/components/Layout';
import { theme } from '/config/theme';
import GlobalStyles from '/components/GlobalStyles';

function MyApp({ Component, pageProps }) {
  return (
    <MainContextProvider initialState={pageProps}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
      <GlobalStyles />
    </MainContextProvider>
  )
}

export default MyApp
