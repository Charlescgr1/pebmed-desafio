import React from 'react';
import dynamic from 'next/dynamic';

const HomeScreen = dynamic(() => import('/screens/Home'));

export default function Home() {
  return <HomeScreen />;
}
// amp on home
export async function getStaticProps() {
  return {
    props: {},
    revalidate: 60 * 5 // 5 minutes
  };
}
