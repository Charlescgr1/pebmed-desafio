const convertToMoney = (value) => value.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
export default convertToMoney;