const getConfig = require('next/config').default;

export const { publicRuntimeConfig = {} } = getConfig ? getConfig() : {};
