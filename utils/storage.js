/**
 * Checks debug is enabled
 *
 * @returns {boolean}
 */

import isBrowser from '/utils/underline/isBrowser';

export const saveStorage = (key, data) => {
  if (isBrowser()) {
    const hasSessionStorage = typeof sessionStorage !== 'undefined';
    if (hasSessionStorage) {
      sessionStorage.setItem(key, JSON.stringify(data));
    }
  }
  return;
};

export const getStorage = (key) => {
  if (isBrowser()) {
    const hasSessionStorage = typeof sessionStorage !== 'undefined';
    if (hasSessionStorage) {
      const data = sessionStorage.getItem(key);
      if (data !== null) {
        return JSON.parse(data);
      }
    }
  }
  return {};
};
