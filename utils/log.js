/* eslint-disable no-console */
import checkDebugEnabled from '/utils/checkDebugEnabled';

const debugStyle = `
    color: #FFF;
    background: #005EE3;
    border-radius: 2px;
    padding: 2px 4px;
  `;

const warnStyle = `
    color: #444;
    background: yellow;
    border-radius: 2px;
    padding: 2px 4px;
  `;

const errorStyle = `
    color: #FFF;
    background: red;
    border-radius: 2px;
    padding: 2px 4px;
  `;

const tagStylePrimary = `
    color: #444;
    background: #dcedc8;
    border-radius: 2px;
    padding: 2px 4px;
  `;

const tagStyleSecondary = `
    color: #444;
    background: #eceff1;
    border-radius: 2px;
    padding: 2px 4px;
  `;

const debugEnabled = checkDebugEnabled();

const _printMsg = (style, ...msg) => {
  if (!debugEnabled) return;
  console.log('%cDebug', style, ...msg);
};

const _printMsgWithTags =
  (tags, tagStyles) =>
  (style, ...msg) => {
    if (!debugEnabled) return;
    console.log(`%cDebug%c ${tags.join('')}`, style, '', ...tagStyles, ...msg);
  };

const _makeTags = (tagTitles, isFirstGroup, isModuleGroup) => {
  const tags = [];
  const tagStyles = [];

  if (!isFirstGroup) {
    tags.push('%c ');
    tagStyles.push('');
  }

  tagTitles.forEach((tag, index, array) => {
    let msg = `%c${tag}`;

    if (isModuleGroup && index === 0) {
      tagStyles.push(tagStylePrimary);
    } else {
      tagStyles.push(tagStyleSecondary);
    }

    if (index !== array.length - 1) {
      msg += '%c ';
      tagStyles.push('');
    }

    tags.push(msg);
  });
  return { tags, tagStyles };
};

const createLogger = (...tagTitles) => {
  let tags = [];
  let tagStyles = [];
  let printMsg = _printMsg;

  if (tagTitles.length >= 1) {
    const t = _makeTags(tagTitles, true, true);
    tags = t.tags;
    tagStyles = t.tagStyles;

    printMsg = _printMsgWithTags(tags, tagStyles);
  }

  return {
    debug(...msg) {
      printMsg(debugStyle, ...msg);
    },

    debugWithTags:
      (...newTags) =>
      (...msg) => {
        const t = _makeTags(newTags, tags.length <= 0, false);
        _printMsgWithTags([...tags, ...t.tags], [...tagStyles, ...t.tagStyles])(
          debugStyle,
          ...msg
        );
      },

    warn(...msg) {
      printMsg(warnStyle, ...msg);
    },

    warnWithTags:
      (...newTags) =>
      (...msg) => {
        const t = _makeTags(newTags, tags.length <= 0, false);
        _printMsgWithTags([...tags, ...t.tags], [...tagStyles, ...t.tagStyles])(
          warnStyle,
          ...msg
        );
      },

    error(...msg) {
      printMsg(errorStyle, ...msg);
    },

    errorWithTags:
      (...newTags) =>
      (...msg) => {
        const t = _makeTags(newTags, tags.length <= 0, false);
        _printMsgWithTags([...tags, ...t.tags], [...tagStyles, ...t.tagStyles])(
          errorStyle,
          ...msg
        );
      }
  };
};

export default createLogger;
