/**
 * Checks debug is enabled
 *
 * @returns {boolean}
 */

import isBrowser from '/utils/underline/isBrowser';

const checkDebugEnabled = () => {
  let debugEnabled = false;

  if (isBrowser()) {
    const hasSessionStorage = typeof sessionStorage !== 'undefined';
    if (hasSessionStorage) {
      const debug = sessionStorage.getItem('debug');
      if (debug !== null) {
        debugEnabled = JSON.parse(debug);
      }
    }

    if (window.location.search.includes('debug=true')) {
      debugEnabled = true;
      if (hasSessionStorage) {
        sessionStorage.setItem('debug', JSON.stringify(true));
      }
    }

    if (window.location.search.includes('debug=false')) {
      debugEnabled = false;
      if (hasSessionStorage) {
        sessionStorage.setItem('debug', JSON.stringify(false));
      }
    }
  }
  return debugEnabled;
};

export default checkDebugEnabled;
