import React from 'react';
import PropTypes from 'prop-types';
import PageWrapper from '/components/PageWrapper';

const Error = ({ title }) => (
  <PageWrapper
    config={{
      title
    }}
  >
    <h1>{'Page error'}</h1>
  </PageWrapper>
);

Error.propTypes = {
  title: PropTypes.string
};

export default Error;
