import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { useMain } from '/contexts/main.context';
import PageWrapper from '/components/PageWrapper';
import Container from '/components/atoms/Container';
import Col from '/components/atoms/Col';
import Box from '/components/atoms/Box';
import Row from '/components/atoms/Row';
import Heading from '/components/atoms/Heading';
import Span from '/components/atoms/Span';
import Image from '/components/atoms/Image';
import A from '/components/atoms/A';
import Button from '/components/atoms/Button';

import { getStorage } from '/utils/storage';
import convertToMoney from '/utils/convertToMoney';

const Congratulations = ({ title }) => {
  const router = useRouter();
  const { subscription, dispatch } = useMain();
  useEffect(() => {
    (async () => {
      const data = await getStorage('subscription');
      dispatch({ type: 'UPDATE', payload: { subscription: data } });
      if (!data) {
        router.push('/assinatura/')
      }
    })();
  }, []);

  return subscription ? (
    <PageWrapper
      config={{
        title
      }}
    >
      <Container className="wrap">
        <Row className={'center'}>
          <Col colSize={'6'} className="plr--x-big">
            <Box>
              <Heading type={'h4'}>{'Parabéns'}</Heading>
              <Span textColor={'grey.4'}>{'Sua assinatura foi realizada com sucesso.'}</Span>
            </Box>
            <Box className={'bs--medium br--medium p--big mtb--x-big'}>
              <Row>
                <Box className={'bc--grey-5 br--medium p--big w--100'}>
                  <Col colSize={'2'}>
                    <Image src={'/icon-success.svg'} alt={'sucess'} width={'40px'} height={'40px'} layout="fixed" />
                  </Col>
                  <Col colSize={'10'}>
                    <Box className={'w--100'}>
                      <Span fontSize={'x-normal'} textColor={'indigo.1'} className={'center mb--normal'}>
                        {`${subscription?.offer?.title} | ${subscription?.offer?.description}`}
                      </Span>
                    </Box>
                    <Box className={'w--100'}>
                      <Span fontSize={'normal'} textColor={'indigo.1'} className={'center'}>
                        {`${convertToMoney((subscription?.offer?.fullPrice * subscription?.offer?.installments) - (subscription?.offer?.discountAmmount * subscription?.offer?.installments))}`}
                        {' | '}
                        {`${subscription?.offer?.installments} X ${convertToMoney((subscription?.offer?.fullPrice - subscription?.offer?.discountAmmount))}`}
                      </Span> 
                    </Box>            
                  </Col>
                </Box>
                <Box className={'pt--big w--100'}>
                  <Span fontSize={'normal'} textColor={'grey.4'} className={'f--left'}>
                    {'E-mail'}
                  </Span>
                  <Span fontSize={'normal'} textColor={'grey.1'} className={'f--right'}>
                    {'fulano@cicrano.com.br'}
                  </Span>
                </Box>
                <Box className={'pt--big w--100'}>
                  <Span fontSize={'normal'} textColor={'grey.4'} className={'f--left'}>
                    {'CPF'}
                  </Span>
                  <Span fontSize={'normal'} textColor={'grey.1'} className={'f--right'}>
                    {subscription?.creditCardCPF}
                  </Span>
                </Box>
              </Row>
            </Box>
            <Box>
              <A textColor={'indigo.1'} fontSize={'small'} className={'fw--bold'}>{'Gerenciar assinatura'}</A>
            </Box>
            <Box className={'pt--big'}>
              <Button
                className={`hover-effect--shadow is--rounded is--medium w--100 center`}
                onClick={() => {
                  router.push('/assinatura/')
                }}
              >
                {'IR PARA A HOME'}
              </Button>
            </Box>
          </Col>
        </Row>
      </Container>  
    </PageWrapper>
  ) : null
};

Congratulations.propTypes = {
  title: PropTypes.string
};

export default Congratulations;
