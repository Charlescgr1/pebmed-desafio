import React from 'react';
import ReactLoading from 'react-loading';
import PageWrapper from '/components/PageWrapper';

const LoadingScreen = () => (
  <PageWrapper
    config={{
      title: 'Loading'
    }}
  >
    <ReactLoading type={'spin'} color={'grey'} height={'100%'} width={'100%'} />
  </PageWrapper>
);

export default LoadingScreen;
