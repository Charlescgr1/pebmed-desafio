import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { v4 as uuid } from 'uuid';
import { useMain } from '/contexts/main.context';
import PageWrapper from '/components/PageWrapper';
import Container from '/components/atoms/Container';
import Col from '/components/atoms/Col';
import Box from '/components/atoms/Box';
import Row from '/components/atoms/Row';
import Heading from '/components/atoms/Heading';
import Span from '/components/atoms/Span';
import Image from '/components/atoms/Image';
import Label from '/components/atoms/Label';
import A from '/components/atoms/A';
import InputField from '/components/atoms/InputField';
import InputSelect from '/components/atoms/InputSelect';
import FormErrors from '/components/atoms/FormErrors';
import Button from '/components/atoms/Button';
import Icon from '/components/atoms/Icon';
import OfferBox from '/components/OfferBox';
import Modal from '/components/atoms/Modal';


import { maskCPF, valideCPF} from '/utils/valideCPF';
import convertToMoney from '/utils/convertToMoney';
import { saveStorage } from '/utils/storage';

import {
  subscription
} from '/lib/apiClient';

const Plans = ({ title, offers }) => {
  const { dispatch } = useMain();
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [submitClicked, setSubmitClicked] = useState(false);
  const [formErrors, setFormErrors] = useState({
    creditCardHolder: false,
    creditCardCPF: false,
    couponCode: true,
    installments: false,
    creditCardNumber: false,
    creditCardExpirationDate: false,
    creditCardCVV: false
  })
  const [offer, setOffer] = useState(offers[0]);
  const [payment, setPayment] = useState({
    creditCardHolder: '',
    creditCardCPF: '',
    couponCode: '',
    installments: 1,
    creditCardNumber: '',
    creditCardExpirationDate: '',
    creditCardCVV: '',
    offerId: offer?.id,
    gateway: 'iugu',
    userId: 1
  }); 

  const updateOffer = (v) => {
    setOffer(v)
    setPayment({
      ...payment,
      offerId: v?.id,
      installments: 1
    });
  }

  const createInstallmentOptions = () => {
    const installments = offer?.installments || 1;
    const price = offer?.fullPrice - offer?.discountAmmount || 0;
    const options = [{ value: 0, label: 'Selecionar' }]
    for (var i = 1; i <= installments; i++) {
      options.push({ value: i, label: `R$ ${i} x ${convertToMoney(price/i)}` });
    }
    return options;
  }

  const validateField = (fieldName, value) => {
    switch(fieldName) {
      case 'creditCardNumber':
        return value.length == 19;
      case 'creditCardExpirationDate':
        return value.length == 7;
      case 'creditCardCVV':
        return value.length == 3; 
      case 'creditCardHolder':
        return value.length > 1;
      case 'creditCardCPF':
        return valideCPF(value);
      case 'installments':
        return value > 0;        
      default:
        return true;    
    }
  }

  const callBackErros = (fieldName, value) => {
    formErrors[fieldName] = value
    setFormErrors({ ...formErrors })
  }

  const maskInput = (value, type) => {
    let v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    let matches;
    let match;
    let parts = '';
    
    switch (type) {
      case 'creditCardNumber':
        matches = v.match(/\d{4,16}/g);
        // eslint-disable-next-line
        match = matches && matches[0] || '';
        parts = [];

        for (let i = 0; i < match.length; i += 4) {
          parts.push(match.substring(i, i + 4));
        }

        if (parts.length) {
          setPayment({
            ...payment,
            creditCardNumber: parts.join(' ')
          });
        } else {
          setPayment({
            ...payment,
            creditCardNumber: value
          });
        }
        break;
      case 'creditCardExpirationDate':
        v = v.replace(/^(\d\d)/g, '$1 / ');
        setPayment({
          ...payment,
          creditCardExpirationDate: v.substring(0, 7)
        });
        break;
      case 'creditCardCVV':
        setPayment({
          ...payment,
          creditCardCVV: v.substring(0, 3)
        });
        break;
      case 'creditCardCPF':
        const creditCardCPF = maskCPF(value);
        setPayment({
          ...payment,
          creditCardCPF
        });
        break;      
      case 'creditCardHolder':
        setPayment({
          ...payment,
          creditCardHolder: value
        });
        break;
      case 'couponCode':
        setPayment({
          ...payment,
          couponCode: value
        });
        break;
      case 'installments':
        setPayment({
          ...payment,
          installments: value
        });
        break;
      default:
        setPayment({
          ...payment,
          creditCardHolder: value
        });
        break;
    }
  };

  const handleSubmit = async () => {
    setSubmitClicked(true)
    let errors = 0;
    Object.keys(formErrors).map((key) => {
      if (!formErrors[key]) {
        errors++;
      }
    })

    if (errors == 0) {
      setSending(true);
      const data = await subscription(payment);
      dispatch({ type: 'UPDATE', payload: { subscription: { ...payment, offer } } });
      saveStorage('subscription', { ...payment, offer });
      router.push('/parabens/')
      setSending(false);
    }
  }

  return (
    <PageWrapper
      config={{
        title
      }}
    >
      <Container className="wrap">
        <Row>
          <Col colSize={'6'} className="plr--x-big">
            <Box>
              <Heading type={'h4'}>{'Estamos quase lá'}</Heading>
            </Box>
            <Box>
              <Span>{'Insira seus dados de pagamento abaixo:'}</Span>
            </Box>
            <Box className={'mt--medium mb--small center'}>            
              <Image src={'/bandeiras-cartoes.svg'} alt={'bandeiras cartoes'} width={'215px'} height={'23.19px'} layout="fixed" />
            </Box>
            <Box className={'center'}>
              <Span fontSize={'x-small'} textColor={'grey.3'}>{'Pagamentos por '}</Span>
              <Image src={'/iugu-logo.svg'} alt={'iugu'} width={'29px'} height={'11px'} layout="fixed" />
            </Box>
            <Box className={'mt--medium mb--small'}>
              <Row>
                <Col colSize={'12'} className={'mb--normal'}>
                  <Label
                    inputId="creditCardNumber"
                    className="d--inline-block w--100"
                  >
                    {'Número do cartão'}
                  </Label>
                  <InputField
                    id="creditCardNumber"
                    name="creditCardNumber"
                    type="text"
                    value={payment.creditCardNumber}
                    maxLength={19}
                    placeholder="0000 0000 0000 0000"
                    onChange={(v) => maskInput(v, 'creditCardNumber')}
                    className="w--100"
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe um cartão válido'}
                    value={payment.creditCardNumber}
                    validate={(v) => validateField('creditCardNumber', v)}
                    callBack={(v) => callBackErros('creditCardNumber', v)}
                  />
                </Col>
                <Col colSize={'6'} className={'pr--medium mb--normal'}>
                  <Label
                    inputId="creditCardExpirationDate"
                    className="d--inline-block w--100"
                  >
                    {'Validade'}
                  </Label>
                  <InputField
                    full
                    id="creditCardExpirationDate"
                    name="creditCardExpirationDate"
                    type="text"
                    value={payment.creditCardExpirationDate}
                    placeholder="00 / 00"
                    onChange={(v) => maskInput(v, 'creditCardExpirationDate')}
                    className="w--100"
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe uma data válida'}
                    value={payment.creditCardExpirationDate}
                    validate={(v) => validateField('creditCardExpirationDate', v)}
                    callBack={(v) => callBackErros('creditCardExpirationDate', v)}
                  />                  
                </Col>
                <Col colSize={'6'} className={'pl--medium mb--normal'}>
                  <Label
                    inputId="creditCardCVV"
                    className="d--inline-block w--100"
                  >
                    {'CVV'}
                  </Label>
                  <InputField
                    full
                    id="creditCardCVV"
                    name="creditCardCVV"
                    type="text"
                    value={payment.creditCardCVV}
                    maxLength={3}
                    placeholder="000"
                    onChange={(v) => maskInput(v, 'creditCardCVV')}
                    className="w--100"
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe um CVV válido'}
                    value={payment.creditCardCVV}
                    validate={(v) => validateField('creditCardCVV', v)}
                    callBack={(v) => callBackErros('creditCardCVV', v)}
                  />                   
                </Col>
                <Col colSize={'12'} className={'mb--normal'}>
                  <Label
                    inputId="creditCardHolder"
                    className="d--inline-block w--100"
                  >
                    {'Nome impresso no cartão'}
                  </Label>
                  <InputField
                    id="creditCardHolder"
                    name="creditCardHolder"
                    type="text"
                    value={payment.creditCardHolder}
                    placeholder="Seu nome"
                    onChange={(v) => maskInput(v, 'creditCardHolder')}
                    className="w--100"
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe um nome válido'}
                    value={payment.creditCardHolder}
                    validate={(v) => validateField('creditCardHolder', v)}
                    callBack={(v) => callBackErros('creditCardHolder', v)}
                  />                   
                </Col>
                <Col colSize={'12'} className={'mb--normal'}>
                  <Label
                    inputId="creditCardCPF"
                    className="d--inline-block w--100"
                  >
                    {'CPF'}
                  </Label>
                  <InputField
                    id="creditCardCPF"
                    name="creditCardCPF"
                    type="text"
                    value={payment.creditCardCPF}
                    maxLength={14}
                    placeholder="000.000.000-00"
                    onChange={(v) => maskInput(v, 'creditCardCPF')}
                    className="w--100"
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe um cpf válido'}
                    value={payment.creditCardCPF}
                    validate={(v) => validateField('creditCardCPF', v)}
                    callBack={(v) => callBackErros('creditCardCPF', v)}
                  />                    
                </Col>
                {offer?.acceptsCoupon && (
                  <Col colSize={'12'}>
                    <Label
                      inputId="couponCode"
                      className="d--inline-block w--100"
                    >
                      {'Cupom'}
                    </Label>
                    <InputField
                      id="couponCode"
                      name="couponCode"
                      type="text"
                      value={payment.couponCode}
                      placeholder="Insira aqui"
                      onChange={(v) => maskInput(v, 'couponCode')}
                      className="mb--normal w--100"
                    />
                  </Col>                  
                )}

                <Col colSize={'12'} className={'mb--normal'}>
                  <Label
                    inputId="installments"
                    className="d--inline-block w--100"
                  >
                    {'Número de parcelas'}
                  </Label>
                  <InputSelect
                    id="installments"
                    name="installments"
                    className="w--100"
                    options={createInstallmentOptions()}
                    onChange={(v) => maskInput(v, 'installments')}
                  />
                  <FormErrors
                    showMessage={submitClicked}
                    message={'Informe o número de parcelas que gostaria'}
                    value={payment.installments}
                    validate={(v) => validateField('installments', v)}
                    callBack={(v) => callBackErros('installments', v)}
                  />                   
                </Col>
                <Col colSize={'12'}>
                  <Button
                    className={`hover-effect--shadow is--rounded is--medium w--100 center`}
                    onClick={() => handleSubmit()}
                    disabled={sending}
                  >
                    {'Finalizar pagamento'}
                  </Button>
                </Col>                
              </Row>
            </Box>
          </Col>
          <Col colSize={'6'} className="plr--x-big">
            <Box>
              <Heading type={'h4'}>{'Confira o seu plano:'}</Heading>
              <Span fontSize={'small'}>{'fulano@cicrano.com.br'}</Span>
            </Box>
            <Box className={'mt--medium mb--small'}>
              <Row>
                {offers.map(v => (
                  <OfferBox
                    key={uuid()}
                    offer={v}
                    selected={offer}
                    installments={payment?.installments}
                    onClick={(v) => updateOffer(v)}
                  />
                ))}
              </Row>
              <Row>
                <Col colSize={'12'}>
                  <Box className={'center'}>
                    <A onClick={() => setShowModal(true)}>
                      <Span fontSize={'small'} textColor={'grey.1'}>
                        {'Sobre a cobrança '}
                        <Icon name="question-mark" size={'small'} color={'grey.1'} />
                      </Span>
                    </A>
                  </Box>
                </Col>
              </Row>
            </Box>
          </Col>
        </Row>
      </Container>
      <Modal rounded isOpen={showModal} id="modal-info" animation="scale-fade" closeModal={() => setShowModal(false)} className="p--big">
        <Box className="modal__forms p--x-big m--x-big">
          <Span>
            {'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}
          </Span>
        </Box>
      </Modal>      
    </PageWrapper>
  )
};

Plans.propTypes = {
  title: PropTypes.string
};

export default Plans;
