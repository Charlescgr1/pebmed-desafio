import React, { useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const MainContext = React.createContext();

const useMain = () => {
  const context = useContext(MainContext);
  if (context === undefined) {
    throw new Error('useMain must be used within a MainContextProvider');
  }
  return context;
};

const MainContextProvider = ({ initialState, children }) => {
  const [state, setState] = useState(initialState);

  const dispatch = (action) => {
    switch (action.type) {
      case 'UPDATE':
        setTimeout(() => {
          setState({
            ...state,
            ...action.payload
          });
        }, 10);
        break;
      default:
        setState(state);
    }
  };

  useEffect(() => {
    setState(initialState);
  }, [initialState]);

  return (
    <MainContext.Provider value={{ ...state, dispatch }}>
      {children}
    </MainContext.Provider>
  );
};

MainContextProvider.propTypes = {
  initialState: PropTypes.object,
  children: PropTypes.any
};

export { MainContext, MainContextProvider, useMain };
