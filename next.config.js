const figlet = require('figlet');

const withPlugins = require('next-compose-plugins');

const envKeys = require('./config');

const getNextHeadersConfig = require('./config/next/next.config.headers');
const getNextRedirectsConfig = require('./config/next/next.config.redirects');

const nextConfiguration = {
  distDir: 'dist',
  basePath: '',
  assetPrefix: '',
  images: {
    loader: 'default',
    deviceSizes: [384, 640, 750, 828, 1080, 1200, 1920],
    imageSizes: [16, 32, 48, 64, 96, 128, 256],
  },
  compress: true,
  trailingSlash: true,
  reactStrictMode: true,
  poweredByHeader: false,
  env: {
    VERSION: envKeys.VERSION,
    APP_NAME: envKeys.APP_NAME,
    APP_LOCALE: envKeys.APP_LOCALE,
    API_URL: envKeys.API_URL
  },
  headers: getNextHeadersConfig,
  publicRuntimeConfig: envKeys,
  redirects: getNextRedirectsConfig
};

figlet(envKeys.APP_NAME, (err, data) => {
  if (err) {
    console.log('Something went wrong...');
    console.dir(err);
    return;
  }
  console.log(data);
  console.log(`Version: ${envKeys.VERSION}`);
});

module.exports = withPlugins(
  [], nextConfiguration
);
